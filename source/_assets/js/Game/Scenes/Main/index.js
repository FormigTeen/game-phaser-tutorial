import Phaser from 'phaser';
import Player from '../../GameObjects/Player';
import Sky from '../../GameObjects/Scenario/Sky';
import Platforms from '../../GameObjects/Scenario/Platforms';
import Stars from "../../GameObjects/Collectors/Stars";
import Score from "../../Texts/Score";
import Bombs from "../../GameObjects/Collectors/Bombs";

export default class Main extends Phaser.Scene {

    preload = () => {
        Sky.preload(this);
        Platforms.preload(this);
        Bombs.preload(this);
        Stars.preload(this);
        Player.preload(this);
    }

    create = () => {
        this.Keyboard = this.input.keyboard.createCursorKeys();
        this.gameObjects = {
            Sky: new Sky({Scene: this, x: 400, y: 300}),
            Score: new Score({Scene: this, x: 16, y: 16}),
            Platforms: new Platforms({Scene: this}),
            Bombs: new Bombs({Scene: this}),
            Player: new Player({Scene: this, x: 400, y: 350, Controller: this.Keyboard}),
            Stars: new Stars({Scene: this, x: 12, y: 0, stepX: 70, visible: true, active: true}),
        };
        this.gameObjects.Player.addCollider(this.gameObjects.Platforms);

        this.gameObjects.Stars.addCollider(this.gameObjects.Platforms);

        this.gameObjects.Bombs.addCollider(this.gameObjects.Platforms);
        this.gameObjects.Bombs.addColliderHit(this.gameObjects.Player);

        this.gameObjects.Player.addOverlap(this.gameObjects.Stars, this.gameObjects.Stars.collectStar);
        this.gameObjects.Player.onNullScreen(this.gameObjects.Stars, this.gameObjects.Bombs);

    }

    update = () => {
        this.gameObjects.Player.update();
    }
}
