import Phaser from 'phaser';

export default class Platforms extends Phaser.Physics.Arcade.StaticGroup {

    static Config = {
        'asset': {
            'name': 'ground',
            'path': 'assets/images/platform.png',
        }
    }

    static preload = (Scene) => {
        Scene.load.image(Platforms.Config.asset.name, Platforms.Config.asset.path);
    }

    constructor({Scene}) {
        super(Scene.physics.world, Scene);
        Scene.sys.updateList.add(this);

        super.create(400, 568, Platforms.Config.asset.name).setScale(2).refreshBody();
        super.create(600, 400, Platforms.Config.asset.name);
        super.create(50, 250, Platforms.Config.asset.name);
        super.create(750, 220, Platforms.Config.asset.name);
    }

    create = () => {
    }

    update = () => {
    }
}
