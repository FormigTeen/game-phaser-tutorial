import Phaser from 'phaser';
import MainScene from './Scenes/Main';

export default class Game extends Phaser.Game {

    static Config = {
            type: Phaser.AUTO,
            width: 800,
            height: 600,
            physics: {
                default: 'arcade',
                arcade: {
                    gravity: {y: 300},
                    debug: false
                }
            },
            scene: new MainScene()
        }

    constructor() {
        super(Game.Config);
    }

}
