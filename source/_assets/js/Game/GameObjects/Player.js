import Phaser from 'phaser';

export default class Player extends Phaser.Physics.Arcade.Sprite{

    static Config = {
        'asset': {
            'name': 'dude',
            'path': 'assets/images/dude.png',
            'frame': {
                'width': 32,
                'height': 48
            },
        },
        'move': {
            'velocity': {
                'x': 160,
                'y': 500
            },
            'bounce': 0.2,
            'gravity': {
                'x': 0,
                'y': 300
            }
        }
    }

    static preload(scene) {
        scene.load.spritesheet(Player.Config.asset.name,
            Player.Config.asset.path,
            { frameWidth: Player.Config.asset.frame.width, frameHeight: Player.Config.asset.frame.height }
        );
    }

    constructor({Scene, x, y, Controller}) {
        super(Scene, x, y, Player.Config.asset.name);

        this.Controller = Controller;
        Scene.sys.displayList.add(this);
        Scene.sys.updateList.add(this);
        Scene.physics.world.enableBody(this, Phaser.Physics.Arcade.DYNAMIC_BODY);
        this.create();
    }

    create = () => {
        this.setBounce(Player.Config.move.bounce);
        this.setCollideWorldBounds(true);
        this.body.setGravityY(Player.Config.move.gravity.y);
        this.#anims();
    }

    update = () => {
        this.#moves();
    }

    addCollider = (obj) => {
        this.scene.physics.add.collider(this, obj);
    }

    addOverlap = (obj, func) => {
        this.scene.physics.add.overlap(this, obj, func, null, this.scene);
    }

    #moveLeft = () => {
        this.setVelocityX(Player.Config.move.velocity.x * -1);
        this.anims.play('left', true);
    }

    #moveRight = () => {
        this.setVelocityX(Player.Config.move.velocity.x);
        this.anims.play('right', true);
    }

    #moveUp = () => {
        this.setVelocityY(Player.Config.move.velocity.y * -1);
    }

    #moveTurn = () => {
        this.setVelocityX(0);
        this.anims.play('turn');
    }

    #moves = () => {
        if (this.Controller.left.isDown) {
            this.#moveLeft();
        }
        else if (this.Controller.right.isDown) {
            this.#moveRight();
        } else {
            this.#moveTurn();
        }
        if (this.Controller.up.isDown && this.body.touching.down) {
            this.#moveUp();
        }
    }

    #anims = () => {
        this.scene.anims.create({
            key: 'left',
            frames: this.scene.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'turn',
            frames: [ { key: 'dude', frame: 4 } ],
            frameRate: 20
        });

        this.scene.anims.create({
            key: 'right',
            frames: this.scene.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });
    }

    dropOnPlayer = (obj) => {
        var x = (this.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
        obj.drop({x: x});
    }

    onNullScreen = (obj, drop) => {
        return obj.onNullScreen() ? this.dropOnPlayer(drop) : false;
    }
}
