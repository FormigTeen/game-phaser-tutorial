import Phaser from 'phaser';

export default class Stars extends Phaser.Physics.Arcade.Group {

    static Config = {
        'asset': {
            'name': 'star',
            'path': 'assets/images/star.png',
        },
        'repeat': 11,
        'score': 5,
    }

    static preload = (Scene) => {
        Scene.load.image(Stars.Config.asset.name, Stars.Config.asset.path);
    }

    constructor({Scene, x, y, stepX, stepY}) {
        super(Scene.physics.world, Scene, {
            key: Stars.Config.asset.name,
            repeat: Stars.Config.repeat,
            setXY: { x: x, y: y, stepX: stepX, stepY: stepY }
        }, null);
        this.scene.sys.updateList.add(this);
        this.create();
    }

    create = () => {
        this.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });
    }

    update = () => {
    }

    addCollider = (obj) => {
        this.scene.physics.add.collider(this, obj);
    }

    collectStar = (collector, obj) => {
        obj.disableBody(true, true);
        this.scene.gameObjects.Score.addValue(Stars.Config.score);
        this.onNullScreen()
    }

    generateStars = () => {
        this.children.iterate(function (child) {
            child.enableBody(true, child.x, 0, true, true);
        });
    }

    onNullScreen = () => {
        if (this.countActive(true) === 0) {
            this.generateStars();
            return true;
        }
        return false;
    }
}
