import Phaser from 'phaser';

export default class Score extends Phaser.GameObjects.Text {

    static Config = {
        fontSize: '32px',
        fill: '#000'
    }

    #score = 0;

    get text() {
        return "Score: " + this.#score;
    }

    constructor({Scene, x, y}) {
        super(Scene, x, y, 'Score; ' + 0 , Score.Config)
        this.scene.sys.displayList.add(this);
    }

    addValue = (value) => {
        this.#score += value;
        this.setText(this.text);
    }
}


