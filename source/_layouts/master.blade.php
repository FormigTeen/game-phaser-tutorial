<!DOCTYPE html>
<html lang="pt-br">
    <head>
        @section('head')
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
        @show
</head>
    <body>
        @section('content')
        @show
    </body>
    @section('script')
        <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
    @show
</html>
