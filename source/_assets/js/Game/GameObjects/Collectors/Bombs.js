import Phaser from 'phaser';

export default class Bombs extends Phaser.Physics.Arcade.Group {

    static Config = {
        'asset': {
            'name': 'bomb',
            'path': 'assets/images/bomb.png',
        },
        'move': {
            'bounce': {
                'min': 0.5,
                'max': 1,
            }
        }
    }

    static preload = (Scene) => {
        Scene.load.image(Bombs.Config.asset.name, Bombs.Config.asset.path);
    }

    constructor({Scene, x, y, stepX, stepY}) {
        super(Scene.physics.world, Scene, null);
        this.scene.sys.updateList.add(this);
        this.create();
    }

    create = () => {
        this.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });
    }

    update = () => {
    }

    onHit = (objHit, bomb) => {
        this.scene.physics.pause();
        objHit.setTint(0xff0000);
        objHit.anims.play('turn');
        this.scene.gameOver = true;
    }

    drop = ({x, y}) => {
        var bomb = super.create(x, 16, Bombs.Config.asset.name);
        bomb.setBounce(Phaser.Math.FloatBetween(Bombs.Config.move.bounce.min, Bombs.Config.move.bounce.max));
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
    }

    addCollider = (obj) => {
        this.scene.physics.add.collider(this, obj);
    }

    addColliderHit = (obj) => {
        this.scene.physics.add.collider(obj, this, this.onHit);
    }

}
