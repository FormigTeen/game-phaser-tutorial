import Phaser from 'phaser';

export default class Sky extends Phaser.GameObjects.Image {

    static Config = {
        'asset': {
            'name': 'sky',
            'path': 'assets/images/sky.png',
        }
    }

    static preload = (scene) => {
        scene.load.image(Sky.Config.asset.name, Sky.Config.asset.path);
    }

    constructor({Scene, x, y}) {
        super(Scene, x, y, Sky.Config.asset.name);
        this.scene.sys.displayList.add(this);
        this.create();
    }

    create = () => {
    }

    update = () => {
    }
}
